var canvas = document.getElementById("canevas"),
    context = canvas.getContext("2d"),
    stars = 200;

canvas.width = window.innerWidth ;
canvas.height = window.innerHeight;

for (var i = 0; i < stars; i++) {
    var x = Math.random() * canvas.offsetWidth;
    y = Math.random() * canvas.offsetHeight,
        radius = Math.random() * 1.2;
    context.beginPath();
    context.arc(x, y, radius, 0, 360);
    context.fillStyle = "hsla(100,100%,50%,0.8)";
    context.fill();
}
var raf; let vx = 5; let vy = 2;
function draw() {
    context.clearRect(0,0, canvas.width, canvas.height);
    ball.draw();
    ball.x += ball.vx;
    ball.y += ball.vy;
    raf = window.requestAnimationFrame(draw);
}

canvas.addEventListener('mouseover', function(e){
    raf = window.requestAnimationFrame(draw);
});

canvas.addEventListener("mouseout",function(e){
    window.cancelAnimationFrame(raf);
});

ball.draw();